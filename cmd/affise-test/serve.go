package main

import (
	"context"
	"golang.org/x/net/netutil"
	"log"
	"net"
	"net/http"
	"time"
)

func serve(ctx context.Context, dc *DependencyContainer) (err error) {
	mux := http.NewServeMux()
	mux.Handle("/", handler(dc))
	l, err := net.Listen("tcp", ":8000")
	if err != nil {
		log.Fatalf("Listen: %v", err)
	}
	defer l.Close()
	l = netutil.LimitListener(l, dc.connectionLimit)
	srv := &http.Server{
		Handler: mux,
	}
	go func() {
		if err = srv.Serve(l); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen:%+s\n", err)
		}
	}()
	log.Printf("server started")
	<-ctx.Done()
	log.Printf("server stopped")
	ctxShutDown, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer func() {
		cancel()
	}()
	if err = srv.Shutdown(ctxShutDown); err != nil {
		log.Fatalf("server Shutdown Failed:%+s", err)
	}
	log.Printf("server exited properly")
	if err == http.ErrServerClosed {
		err = nil
	}
	return
}
