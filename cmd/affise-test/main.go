package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
)

type DependencyContainer struct {
	maxLimitUrls    int
	connectionLimit int
	limitWorker     int
	timeout         int
}

func main() {
	dc := &DependencyContainer{}
	dc.maxLimitUrls = *flag.Int("max_limit_urls", 20, "")
	dc.connectionLimit = *flag.Int("connection_limit", 100, "")
	dc.limitWorker = *flag.Int("limit_worker", 4, "")
	dc.timeout = *flag.Int("timeout", 60, "")
	flag.Parse()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		oscall := <-c
		log.Printf("system call:%+v", oscall)
		cancel()
	}()
	if err := serve(ctx, dc); err != nil {
		log.Printf("failed to serve:+%v\n", err)
	}
}
