package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"
)

func handler(dc *DependencyContainer) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		if r.Method != http.MethodPost {
			JSONError(w, "", http.StatusMethodNotAllowed)
			return
		}
		var request struct {
			Urls []string `json:"urls"`
		}
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			JSONError(w, fmt.Sprintf("Error detect body %s", err.Error()), http.StatusBadRequest)
			return
		}
		//todo check duplicate values
		if len(request.Urls) > dc.maxLimitUrls {
			JSONError(w, fmt.Sprintf("Urls limit exceeded, maximum limit %d", dc.maxLimitUrls), http.StatusBadRequest)
			return
		}
		client := &http.Client{
			Timeout: time.Duration(dc.timeout) * time.Second,
		}
		response := struct {
			Urls map[string]int `json:"urls"`
			sync.RWMutex
		}{
			Urls: make(map[string]int),
		}
		urls := make(chan string)
		go func() {
			for _, u := range request.Urls {
				urls <- u
			}
			close(urls)
		}()
		var wg sync.WaitGroup
		for i := 0; i < dc.limitWorker; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				for url := range urls {
					select {
					case <-ctx.Done():
						log.Printf("%s", ctx.Err().Error())
						return
					default:
					}
					log.Printf("Send response %s", url)
					req, err := http.NewRequest(http.MethodHead, url, nil)
					if err != nil {
						JSONError(w, err.Error(), http.StatusInternalServerError)
						cancel()
						return
					}
					res, err := client.Do(req)
					if err != nil {
						JSONError(w, err.Error(), http.StatusInternalServerError)
						cancel()
						return
					}
					if res.StatusCode != http.StatusOK {
						JSONError(w, fmt.Sprintf("Status error %s %d", url, res.StatusCode), http.StatusInternalServerError)
						cancel()
						return
					}
					////todo change format response body or head?
					response.Lock()
					response.Urls[url] = res.StatusCode
					response.Unlock()
				}
			}()
		}
		wg.Wait()
		if ctx.Err() != nil {
			return
		}
		defer response.RUnlock()
		response.RLock()
		jsonBytes, err := json.Marshal(response)
		if err != nil {
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		_, _ = fmt.Fprintf(w, "%s", string(jsonBytes))
		//todo check OOM
	})
}
